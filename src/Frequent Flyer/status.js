const SILVER = 'Silver';
const GOLD = 'Gold';
const PLATINUM = 'Platinum';
const DIAMOND = 'Diamond';
const MEMBER = 'Member';

const get = ({ miles, segments, dollars }) => {
  if ((miles >= 125000 || segments >= 140) && dollars >= 12500) {
    return DIAMOND;
  }
  if ((miles >= 75000 || segments >= 100) && dollars >= 7500) {
    return PLATINUM;
  }
  if ((miles >= 50000 || segments >= 60) && dollars >= 5000) {
    return GOLD;
  }
  if ((miles >= 25000 || segments >= 30) && dollars >= 2500) {
    return SILVER;
  }

  return MEMBER;
};

export {
  get,
  SILVER,
  GOLD,
  PLATINUM,
  DIAMOND,
  MEMBER,
};
