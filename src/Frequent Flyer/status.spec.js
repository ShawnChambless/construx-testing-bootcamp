import {
  get, DIAMOND, PLATINUM, GOLD, SILVER, MEMBER,
} from './status';

describe('Status Tests', () => {
  describe('Silver status', () => {
    it('Returns silver status when ALL boundary conditions are met', () => {
      const miles = 25000;
      const segments = 30;
      const dollars = 2500;

      const status = get({ miles, segments, dollars });
      expect(status).toBe(SILVER);
    });

    it('Returns silver for enough segments, not enough miles, and enough dollars', () => {
      const miles = 24999;
      const segments = 30;
      const dollars = 2500;

      const status = get({ miles, segments, dollars });
      expect(status).toBe(SILVER);
    });

    it('Returns silver for not enough segments, enough miles, and enough dollars', () => {
      const miles = 25000;
      const segments = 29;
      const dollars = 2500;

      const status = get({ miles, segments, dollars });
      expect(status).toBe(SILVER);
    });
  });

  describe('Gold status', () => {
    it('Returns gold status when ALL boundary conditions are met', () => {
      const miles = 50000;
      const segments = 60;
      const dollars = 5000;

      const status = get({ miles, segments, dollars });
      expect(status).toBe(GOLD);
    });

    it('Returns gold for enough segments, not enough miles, and enough dollars', () => {
      const miles = 49999;
      const segments = 60;
      const dollars = 5000;

      const status = get({ miles, segments, dollars });
      expect(status).toBe(GOLD);
    });

    it('Returns gold for not enough segments, enough miles, and enough dollars', () => {
      const miles = 50000;
      const segments = 59;
      const dollars = 5000;

      const status = get({ miles, segments, dollars });
      expect(status).toBe(GOLD);
    });

    describe('Platinum status', () => {
      it('Returns platinum status when ALL boundary conditions are met', () => {
        const miles = 75000;
        const segments = 100;
        const dollars = 7500;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(PLATINUM);
      });

      it('Returns platinum for enough segments, not enough miles, and enough dollars', () => {
        const miles = 74999;
        const segments = 100;
        const dollars = 7500;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(PLATINUM);
      });

      it('Returns platinum for not enough segments, enough miles, and enough dollars', () => {
        const miles = 75000;
        const segments = 99;
        const dollars = 7500;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(PLATINUM);
      });
    });

    describe('Diamond status', () => {
      it('Returns diamond status when ALL boundary conditions are met', () => {
        const miles = 125000;
        const segments = 140;
        const dollars = 12500;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(DIAMOND);
      });

      it('Returns diamond for enough segments, not enough miles, and enough dollars', () => {
        const miles = 124999;
        const segments = 140;
        const dollars = 12500;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(DIAMOND);
      });

      it('Returns diamond for not enough segments, enough miles, and enough dollars', () => {
        const miles = 125000;
        const segments = 139;
        const dollars = 12500;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(DIAMOND);
      });
    });

    describe('Member tests', () => {
      it('Returns member if both miles and segments meet a threshold, but dollars do not', () => {
        const miles = 25000;
        const segments = 30;
        const dollars = 2499;

        const status = get({ miles, segments, dollars });
        expect(status).toBe(MEMBER);
      });
    });
  });
});
