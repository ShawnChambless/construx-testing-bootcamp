import SecretSanta from './secretSanta';

describe('Secret Santa', () => {
  let santa;
  beforeEach(() => {
    santa = new SecretSanta();
  });

  it('returns a list of secret santas', () => {
    expect(santa.getCount()).toBe(0);
  });

  it('adds participants to santas list', () => {
    santa.addSanta('jane');
    expect(santa.getCount()).toBe(1);
  });

  it('throws an exception when there are not more than 4 participants', () => {
    santa.addSanta('jane');
    santa.addSanta('brad');
    santa.addSanta('carmen');
    expect(() => santa.getSantas()).toThrow();
  });

  it('returns pairs when length >= 4', () => {
    santa.addSanta('jane');
    santa.addSanta('brad');
    santa.addSanta('carmen');
    santa.addSanta('jacob');

    const santas = santa.getSantas();
    expect(santas.length).toBe(2);
  });

  it('does not have self-santas', () => {
    santa.addSanta('jane');
    santa.addSanta('brad');
    santa.addSanta('carmen');
    santa.addSanta('jacob');

    const santas = santa.getSantas();
    const containsSelfSantas = santas.find(a => a.length > 1 && a[0] === a[1]);

    expect(!containsSelfSantas).toBeTruthy()
  })
});