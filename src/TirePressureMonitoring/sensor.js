class Sensor {
  static ReadPressureSample() {
    return Math.floor(6 * Math.random() * Math.random());
  }

  static Offset() {
    return 16;
  }

  static popNextPressurePsiValue() {
    const pressureTelemetryValue = Sensor.ReadPressureSample();

    return Sensor.Offset() + pressureTelemetryValue;
  }
}

export default Sensor;
