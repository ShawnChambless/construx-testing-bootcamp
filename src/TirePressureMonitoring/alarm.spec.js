import Alarm from './alarm';
import Sensor from './sensor';

describe('Tyre Pressure Monitoring System', () => {
  describe('Alarm', () => {
    let sensor;
    beforeEach(() => {
      sensor = Sensor;
    });

    it('Alarm is on above upper threshold', () => {
      sensor.popNextPressurePsiValue = () => 22;
      const target = new Alarm(sensor);
      target.check();
      expect(target._alarmOn).toBe(true);
    });

    it('Alarm is on below lower threshold', () => {
      sensor.popNextPressurePsiValue = () => 16;
      const target = new Alarm(sensor);
      target.check();
      expect(target._alarmOn).toBe(true);
    });

    it('Alarm is off at upper threshold', () => {
      sensor.popNextPressurePsiValue = () => 21;
      const target = new Alarm(sensor);
      target.check();
      expect(target._alarmOn).toBe(false);
    });

    it('Alarm is off at lower threshold', () => {
      sensor.popNextPressurePsiValue = () => 17;
      const target = new Alarm(sensor);
      target.check();
      expect(target._alarmOn).toBe(false);
    });
  });
});
