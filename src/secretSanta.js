class SecretSanta {
  santas = [];

  getCount() {
    return this.santas.length;
  }

  getSantas() {
    if(this.santas.length < 4) {
      throw new Error('Santas must contain more than 4 participants')
    }

    const formPairs = ([head, ...tail], pairs = []) => {
      if(!head) {
        return pairs;
      }
      if(!tail.length) {
        return [...pairs, head];
    }

      const rand = Math.floor(Math.random() * tail.length);
      return formPairs(tail, [...pairs, [head, tail.splice(rand, 1)[0]]])
    };

    return formPairs(this.santas, [])
  }

  addSanta(participant) {
    this.santas.push(participant)
  }
}

export default SecretSanta